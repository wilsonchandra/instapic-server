import json
import uuid
import base64
import psycopg2
from psycopg2.extras import RealDictCursor
from flask import Flask, request, jsonify, send_from_directory
from flask_cors import CORS
app = Flask(__name__, static_url_path='')
CORS(app)

conn = psycopg2.connect("dbname='instapic' user='postgres' host='localhost' password='wilsonch'")
imgfolder = "files/"

@app.route('/')
def index():
    return 'Server Works!'
  
@app.route('/feed')
def feed():
    cur = conn.cursor(cursor_factory=RealDictCursor)
    feedquery = """
        SELECT * FROM public.posts 
        ORDER BY datetime DESC;
    """
    cur.execute(feedquery)
    return json.dumps(cur.fetchall(), default=str)

@app.route('/user/<name>')
def user(name):
    cur = conn.cursor(cursor_factory=RealDictCursor)
    username = name
    feedquery = """
        SELECT * FROM public.posts WHERE username = %s
        ORDER BY datetime DESC;
    """
    cur.execute(feedquery, (username,))
    return json.dumps(cur.fetchall(), default=str)

@app.route('/upload', methods=['POST'])
def upload():
    cur = conn.cursor()
    uploadjson = request.json
    username = uploadjson['username']
    description = uploadjson['description']
    filename = uuid.uuid4().hex + '.' + uploadjson['filename'].split(".")[-1]

    filecontent=base64.b64decode(uploadjson['filebase64'].split("base64,")[-1])
    with open(imgfolder + filename,"wb") as f:
        f.write(filecontent)
    
    uploadquery = """
        INSERT INTO public.posts(
	    username, description, filename)
	    VALUES (%s, %s, %s);
    """
    cur.execute(uploadquery, (username,description,filename))
    conn.commit()
    cur.close()

    return jsonify({"msg": "File upload success", "status": 200})

@app.route('/files/<path:path>')
def send_files(path):
    return send_from_directory('files', path)
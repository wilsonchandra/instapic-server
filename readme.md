Server for Instapic

How to set up database:
1. Install postgresql v9.6
2. Assign postgres user password of 'wilsonch'
3. Using psql, create database 'instapic' and import instapic.sql
4. Using psql, on database 'instapic', do 'select * from public.posts' to verify

How to set up instapic-server dependencies:
1. Make sure database is setted up
2. Using pip, install flask, flask_cors and psycopg2

How to run instapic-server
1. Run 'flask run --host=0.0.0.0'

API Reference:
1. GET /feed
Returns: JSON array of public.posts database

2. GET /user/<name>
Where <name> is the username
Returns: JSON array of public.posts database filtered by user

3. POST /upload
Accepts: JSON of username, description, filename, filebase64
Where filebase64 is the base64 encoded file to be uploaded
Returns: JSON msg of upload status and status of HTTP status